const cloud = require('wx-server-sdk')
cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
const _ = db.command
exports.main = async (event, context) => {
  try {
    return await db.collection('teacher').where({
      _id: event.id
    })
      .update({
        data: {
          name: event.name,
          no: event.no,
          course: event.course,
          sex: event.sex,
          year: event.year,
          grade: event.grade,
          score: event.score,
          cost: event.cost,
          detail:event.detail,
          area:event.area
        },
      })
  } catch (e) {
    console.error(e)
  }
}
