// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
const order = db.collection('order')
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  var currentDate = getNowFormatDate();
  let num = 0;
  let orderno =""
  await order.where({
    createDate: currentDate
  }).count().then(res=>{
    num=res.total+1
    if (num >= 1 && num <= 9) {
      orderno = currentDate + "00" + num;
    } else if (num >= 10 && num <= 99) {
      orderno = currentDate + "0" + num;
    } else if (num >= 100 && num <= 999) {
      orderno = currentDate + num;
    }
  });
  return orderno;
}
function getNowFormatDate() {
  var date = new Date();
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var strDate = date.getDate();
  if (month >= 1 && month <= 9) {
    month = "0" + month;
  }
  if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
  }
  var currentdate = year + month + strDate;
  return currentdate;
}