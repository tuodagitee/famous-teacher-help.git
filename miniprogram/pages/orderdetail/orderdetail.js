// miniprogram/pages/orderdetail/orderdetail.js
wx.cloud.init()
const db = wx.cloud.database()
const subscribeCollection = db.collection('subscribe')
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    subscribe:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.flag)
    console.log(subscribeCollection.doc(options.id))
    if(options.flag=='1'){
    wx.cloud.callFunction({
      name: 'updateSucribe',
      data: {
        id:options.id
      }
    })
    }
    subscribeCollection.doc(options.id).get({
      success: res => {
        console.log(res.data)
        this.setData({
          subscribe: res.data
        })
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})