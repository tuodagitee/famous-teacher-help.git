wx.cloud.init()
const db = wx.cloud.database()
const subscribeCollection = db.collection('subscribe')
const _ = db.command
const app = getApp();
Page({
  data: {
    subcribe:{}
  },
  onLoad: function () {
    var openid = "";
    openid = app.globalData.userInfo.openid
    console.log(openid)
    subscribeCollection.orderBy('createtime', 'desc').where({
      _openid: openid
    }).get().then(res => {
      for(var index in res.data){
        var time = res.data[index].createtime;
        console.log(time)
        res.data[index].createtime = time.getFullYear() + "-" + (time.getMonth() + 1) + "-" + time.getDate()
      }
      
      this.setData({
        subscribe: res.data,
      })
    })
    this.setData({
      slideButtons: [ {
        type: 'warn',
        text: '删除',
        extClass: 'test',
        src: '/page/weui/cell/icon_del.svg', // icon的路径
      }],
    });
  },
  slideButtonTap(e) {
    console.log('slide button tap', e.detail)
  }
});