// miniprogram/pages/teacherdetail/teacherdetail.js
wx.cloud.init()
const db = wx.cloud.database()
const teacherCollection = db.collection('teacher')
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: { 
    teacher:[],
    indicatorDots: true, //是否显示面板指示点
    autoplay: true, //是否自动切换
    interval: 3000, //自动切换时间间隔,3s
    duration: 1000, //  滑动动画时长1s
    detail: "",
    hasimage:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.id)
     teacherCollection.doc(options.id).get({
      success: res => {
        console.log(res.data.detail)
        if(res.data.detail!=undefined){
          if(res.data.detail.length>0){
            this.data.hasimage=true
          }
        }
        this.setData({
          teacher:res.data,
          hasimage:this.data.hasimage
        })
      }
     })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getUserInfo: function (result) {
    console.log("111")
    wx.redirectTo({
      url: '../order/order',
    })
  }
})