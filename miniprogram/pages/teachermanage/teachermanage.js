// miniprogram/pages/teacherlist/teacherlist.js
wx.cloud.init()
const db = wx.cloud.database()
const teacherCollection = db.collection('teacher')
const _ = db.command
Page({

  /**
   * 页面的初始数据
   */
  data: {
    teacherlist: [],
    type: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.name + " " + options.type)
    console.log(teacherCollection)
    teacherCollection.where({
      course: options.name,
    }).get().then(res => {
      console.log(res.data)
      this.setData({
        teacherlist: res.data,
      })
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },newPost: function (e) {
    wx.navigateTo({
      url: '../addteacher/addteacher'
    })
  },
  slideButtonTap(e) {
    var index = e.detail.index;
    console.log(index)
    if(index ==1){
      wx.cloud.callFunction({
        name: 'removeTeacher',
        data: {
          id: e.detail.data._id,
        }
      }).then(res => {
        wx.showToast({
          title: '删除成功',
          icon: 'success',
          duration: 2000
        })
        wx.redirectTo({
          url: '../teachermanage/teachermanage',
        })
      }).catch(err => {
        // handle error
      })
    }else{
      console.log(index)
      wx.redirectTo({
        url: '../editteacher/editteacher?id=' + e.detail.data._id,
      })
    }
    console.log('slide button tap', e.detail.data._id)
  }
})